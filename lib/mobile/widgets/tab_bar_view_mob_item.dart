import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TabBarViewMobItem extends StatelessWidget {
  const TabBarViewMobItem({
    super.key,
    required this.title,
    required this.firstStepTitle,
    required this.secondStepTitle,
    required this.thirdStepTitle,
    required this.secondImagePath,
    required this.thirdImagePath,
    required this.thirdStepTitlePaddingTop,
    required this.secondStepTitlePaddingLeft,
    required this.thirdImagePaddingLeft,
    required this.thirdImagePaddingTop,
    required this.isStacked,
  });

  final String title;
  final String firstStepTitle;
  final String secondStepTitle;
  final String thirdStepTitle;
  final String secondImagePath;
  final String thirdImagePath;
  final double thirdStepTitlePaddingTop;
  final double secondStepTitlePaddingLeft;
  final double thirdImagePaddingTop;
  final double thirdImagePaddingLeft;
  final bool isStacked;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Center(
            child: Text(
              title,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 21,
                color: Color(0xff4A5568),
                height: 1.19,
              ),
            ),
          ),
        ),
        Stack(
          clipBehavior: Clip.none,
          children: [
            Transform.translate(
              offset: const Offset(-34, 77),
              child: Container(
                width: 208,
                height: 208,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0xffF7FAFC).withOpacity(0.5),
                ),
              ),
            ),
            Positioned(
              top: 20.06,
              left: 100.44,
              child: SvgPicture.asset('assets/images/2.svg'),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 106, left: 12),
                  child: Text(
                    '1.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 130,
                      color: Color(0xff718096),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 23, bottom: 27),
                  child: Text(
                    firstStepTitle,
                    textAlign: TextAlign.left,
                    style: const TextStyle(
                      fontSize: 16,
                      color: Color(0xff718096),
                    ),
                  ),
                ),
              ],
            ),
            Transform.translate(
              offset: const Offset(-54, 579),
              child: Container(
                width: 304,
                height: 303,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffF7FAFC),
                ),
              ),
            ),
            const Positioned(
              top: 579,
              left: 56,
              child: Text(
                '3.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 130,
                  color: Color(0xff718096),
                ),
              ),
            ),
            Positioned(
              top: thirdStepTitlePaddingTop,
              left: 181,
              child: Text(
                thirdStepTitle,
                textAlign: TextAlign.left,
                style: const TextStyle(
                  fontSize: 16,
                  color: Color(0xff718096),
                ),
              ),
            ),
            Positioned(
              top: thirdImagePaddingTop,
              left: thirdImagePaddingLeft,
              child: SvgPicture.asset('assets/images/$thirdImagePath.svg'),
            ),
            Positioned(
              top: 257.06,
              child: ClipPath(
                clipper: WaveClipperTwo(flip: false, reverse: true),
                child: ClipPath(
                  clipper: WaveClipperOne(flip: false, reverse: false),
                  child: Container(
                    height: 370,
                    width: width,
                    decoration: const BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topRight,
                        end: Alignment.bottomLeft,
                        colors: [
                          Color(0xffEBF4FF),
                          Color(0xffE6FFFA),
                        ],
                      ),
                    ),
                    child: isStacked
                        ? Stack(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                  left: secondStepTitlePaddingLeft,
                                  top: 120,
                                ),
                                child: SvgPicture.asset(
                                    'assets/images/$secondImagePath.svg'),
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left: 37),
                                    child: Text(
                                      '2.',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 130,
                                        color: Color(0xff718096),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 23,
                                      bottom: 27,
                                    ),
                                    child: Text(
                                      secondStepTitle,
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff718096),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          )
                        : Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const Padding(
                                    padding: EdgeInsets.only(left: 37),
                                    child: Text(
                                      '2.',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 130,
                                        color: Color(0xff718096),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      left: 23,
                                      bottom: 27,
                                    ),
                                    child: Text(
                                      secondStepTitle,
                                      textAlign: TextAlign.left,
                                      style: const TextStyle(
                                        fontSize: 16,
                                        color: Color(0xff718096),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Padding(
                                padding: EdgeInsets.only(
                                  left: secondStepTitlePaddingLeft,
                                ),
                                child: SvgPicture.asset(
                                    'assets/images/$secondImagePath.svg'),
                              ),
                            ],
                          ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
