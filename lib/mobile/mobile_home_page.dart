import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_test_index/mobile/widgets/tab_bar_view_mob_item.dart';

class MobileHomePage extends StatefulWidget {
  const MobileHomePage({super.key});

  @override
  State<MobileHomePage> createState() => _MobileHomePageState();
}

class _MobileHomePageState extends State<MobileHomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _tabIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
        ),
        toolbarHeight: 67,
        flexibleSpace: Column(
          children: [
            Container(
              height: 5,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xff3182CE),
                    Color(0xff319795),
                  ],
                ),
              ),
            ),
            Container(
              height: 62,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12),
                ),
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: const Text(
              'Login',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: Color(0xff319795),
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: Container(
        width: width,
        height: 88,
        decoration: const BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color(0xff707070),
              spreadRadius: 1,
              blurRadius: 5,
              offset: Offset(0, 3),
            ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12),
            topRight: Radius.circular(12),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 24),
              child: InkWell(
                onTap: () {},
                child: Container(
                  width: 320,
                  height: 40,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Color(0xff3182CE),
                        Color(0xff319795),
                      ],
                    ),
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                  ),
                  alignment: Alignment.center,
                  child: const Text(
                    'Kostenlos Registrieren',
                    style: TextStyle(
                      fontSize: 14,
                      fontWeight: FontWeight.w600,
                      color: Color(0xffE6FFFA),
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            ClipPath(
              clipper: WaveClipperTwo(),
              child: Container(
                width: width,
                height: height,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xffEBF4FF),
                      Color(0xffE6FFFA),
                    ],
                  ),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 18),
                      child: SizedBox(
                        width: 320,
                        child: Text(
                          'Deine Job website',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 42,
                            color: Color(0xff2D3748),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 2),
                      child: SvgPicture.asset(
                        'assets/images/1.svg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 1200,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  StatefulBuilder(builder: (context, setState) {
                    return TabBar(
                      controller: _tabController,
                      onTap: (index) {
                        setState(() {
                          _tabIndex = index;
                        });
                      },
                      padding: const EdgeInsets.symmetric(
                        horizontal: 20,
                        vertical: 27,
                      ),
                      labelPadding: EdgeInsets.zero,
                      indicatorPadding: EdgeInsets.zero,
                      indicatorColor: Colors.transparent,
                      tabs: [
                        Container(
                          width: 160,
                          height: 40,
                          decoration: BoxDecoration(
                            color: _tabIndex == 0
                                ? const Color(0xff81E6D9)
                                : Colors.white,
                            border: _tabIndex == 0
                                ? null
                                : Border.all(
                                    color: const Color(0xffCBD5E0),
                                  ),
                            borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(12),
                                bottomLeft: Radius.circular(12)),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            'Arbeitnehmer',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: _tabIndex == 0
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                        Container(
                          width: 160,
                          height: 40,
                          decoration: BoxDecoration(
                            color: _tabIndex == 1
                                ? const Color(0xff81E6D9)
                                : Colors.white,
                            border: _tabIndex == 1
                                ? null
                                : Border.all(
                                    color: const Color(0xffCBD5E0),
                                  ),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            'Arbeitnehmer',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: _tabIndex == 1
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                        Container(
                          width: 160,
                          height: 40,
                          decoration: BoxDecoration(
                            color: _tabIndex == 2
                                ? const Color(0xff81E6D9)
                                : Colors.white,
                            border: _tabIndex == 2
                                ? null
                                : Border.all(
                                    color: const Color(0xffCBD5E0),
                                  ),
                            borderRadius: const BorderRadius.only(
                                topRight: Radius.circular(12),
                                bottomRight: Radius.circular(12)),
                          ),
                          alignment: Alignment.center,
                          child: Text(
                            'Temporärbüro',
                            style: TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.w600,
                              color: _tabIndex == 2
                                  ? const Color(0xffE6FFFA)
                                  : const Color(0xff319795),
                            ),
                          ),
                        ),
                      ],
                    );
                  }),
                  Expanded(
                    child: TabBarView(
                      controller: _tabController,
                      physics: const NeverScrollableScrollPhysics(),
                      children: const [
                        TabBarViewMobItem(
                          title: 'Drei einfache Schritte\nzu deinem neuen Job',
                          firstStepTitle: 'Erstellen dein Lebenslauf',
                          secondStepTitle: 'Erstellen dein Lebenslauf',
                          thirdStepTitle: 'Mit nur einem Klick\nbewerben',
                          secondImagePath: '3',
                          thirdImagePath: '4',
                          thirdStepTitlePaddingTop: 648,
                          secondStepTitlePaddingLeft: 112.26,
                          thirdImagePaddingTop: 695.35,
                          thirdImagePaddingLeft: 72,
                          isStacked: false,
                        ),
                        TabBarViewMobItem(
                          title:
                              'Drei einfache Schritte\nzu deinem neuen Mitarbeiter',
                          firstStepTitle: 'Erstellen dein\nUnternehmensprofil',
                          secondStepTitle: 'Erstellen ein Jobinserat',
                          thirdStepTitle: 'Wähle deinen\nneuen Mitarbeiter aus',
                          secondImagePath: '5',
                          thirdImagePath: '6',
                          thirdStepTitlePaddingTop: 658,
                          secondStepTitlePaddingLeft: 58.52,
                          thirdImagePaddingTop: 752.46,
                          thirdImagePaddingLeft: 59.6,
                          isStacked: true,
                        ),
                        TabBarViewMobItem(
                          title:
                              'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
                          firstStepTitle: 'Erstellen dein\nUnternehmensprofil',
                          secondStepTitle:
                              'Erhalte Vermittlungs-\nangebot von Arbeitgeber',
                          thirdStepTitle:
                              'Vermittlung nach\nProvision oder\nStundenlohn',
                          secondImagePath: '7',
                          thirdImagePath: '8',
                          thirdStepTitlePaddingTop: 653,
                          secondStepTitlePaddingLeft: 102.37,
                          thirdImagePaddingTop: 739,
                          thirdImagePaddingLeft: 55,
                          isStacked: false,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
