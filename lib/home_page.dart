import 'package:flutter/material.dart';
import 'package:flutter_test_index/desktop/desktop_home_page.dart';
import 'package:flutter_test_index/mobile/mobile_home_page.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (constraints.maxWidth > 480) {
          return const DesktopHomePage();
        } else {
          return const MobileHomePage();
        }
      },
    );
  }
}
