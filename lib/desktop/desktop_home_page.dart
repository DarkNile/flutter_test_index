import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_test_index/desktop/widgets/tab_bar_view_desktop_item.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';

class DesktopHomePage extends StatefulWidget {
  const DesktopHomePage({super.key});

  @override
  State<DesktopHomePage> createState() => _DesktopHomePageState();
}

class _DesktopHomePageState extends State<DesktopHomePage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  int _tabIndex = 0;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(12),
            bottomRight: Radius.circular(12),
          ),
        ),
        toolbarHeight: 67,
        flexibleSpace: Column(
          children: [
            Container(
              height: 5,
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Color(0xff3182CE),
                    Color(0xff319795),
                  ],
                ),
              ),
            ),
            Container(
              height: 62,
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(12),
                  bottomRight: Radius.circular(12),
                ),
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {},
            child: const Text(
              'Login',
              style: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w600,
                color: Color(0xff319795),
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: SizedBox(
          height: 2400,
          child: Column(
            children: [
              ClipPath(
                clipper: WaveClipperTwo(),
                child: Container(
                  width: width,
                  height: 597.37,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [
                        Color(0xffE6FFFA),
                        Color(0xffEBF4FF),
                      ],
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const SizedBox(
                            width: 320,
                            child: Text(
                              'Deine Job website',
                              textAlign: TextAlign.left,
                              style: TextStyle(
                                fontSize: 65,
                                color: Color(0xff2D3748),
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 53),
                            child: InkWell(
                              onTap: () {},
                              child: Container(
                                width: 320,
                                height: 40,
                                decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                    begin: Alignment.topRight,
                                    end: Alignment.bottomLeft,
                                    colors: [
                                      Color(0xff3182CE),
                                      Color(0xff319795),
                                    ],
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(12)),
                                ),
                                alignment: Alignment.center,
                                child: const Text(
                                  'Kostenlos Registrieren',
                                  style: TextStyle(
                                    fontSize: 14,
                                    fontWeight: FontWeight.w600,
                                    color: Color(0xffE6FFFA),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        width: 455,
                        height: 455,
                        margin: const EdgeInsets.only(left: 151),
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: Svg('assets/images/1.svg'),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              TabBar(
                controller: _tabController,
                onTap: (index) {
                  setState(() {
                    _tabIndex = index;
                  });
                },
                padding: EdgeInsets.only(
                  top: 35.63,
                  left: width / 2 - 240,
                  right: width / 2 - 240,
                ),
                labelPadding: EdgeInsets.zero,
                indicatorPadding: EdgeInsets.zero,
                indicatorColor: Colors.transparent,
                tabs: [
                  Container(
                    width: 160,
                    height: 40,
                    decoration: BoxDecoration(
                      color: _tabIndex == 0
                          ? const Color(0xff81E6D9)
                          : Colors.white,
                      border: _tabIndex == 0
                          ? null
                          : Border.all(
                              color: const Color(0xffCBD5E0),
                            ),
                      borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(12),
                          bottomLeft: Radius.circular(12)),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      'Arbeitnehmer',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: _tabIndex == 0
                            ? const Color(0xffE6FFFA)
                            : const Color(0xff319795),
                      ),
                    ),
                  ),
                  Container(
                    width: 160,
                    height: 40,
                    decoration: BoxDecoration(
                      color: _tabIndex == 1
                          ? const Color(0xff81E6D9)
                          : Colors.white,
                      border: _tabIndex == 1
                          ? null
                          : Border.all(
                              color: const Color(0xffCBD5E0),
                            ),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      'Arbeitnehmer',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: _tabIndex == 1
                            ? const Color(0xffE6FFFA)
                            : const Color(0xff319795),
                      ),
                    ),
                  ),
                  Container(
                    width: 160,
                    height: 40,
                    decoration: BoxDecoration(
                      color: _tabIndex == 2
                          ? const Color(0xff81E6D9)
                          : Colors.white,
                      border: _tabIndex == 2
                          ? null
                          : Border.all(
                              color: const Color(0xffCBD5E0),
                            ),
                      borderRadius: const BorderRadius.only(
                          topRight: Radius.circular(12),
                          bottomRight: Radius.circular(12)),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      'Temporärbüro',
                      style: TextStyle(
                        fontSize: 14,
                        fontWeight: FontWeight.w600,
                        color: _tabIndex == 2
                            ? const Color(0xffE6FFFA)
                            : const Color(0xff319795),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: TabBarView(
                  controller: _tabController,
                  physics: const NeverScrollableScrollPhysics(),
                  children: const [
                    TabBarViewDesktopItem(
                      title: 'Drei einfache Schritte\nzu deinem neuen Job',
                      firstStepTitle: 'Erstellen dein Lebenslauf',
                      secondStepTitle: 'Erstellen dein Lebenslauf',
                      thirdStepTitle: 'Mit nur einem Klick\nbewerben',
                      secondImagePath: '3',
                      thirdImagePath: '4',
                    ),
                    TabBarViewDesktopItem(
                      title:
                          'Drei einfache Schritte\nzu deinem neuen Mitarbeiter',
                      firstStepTitle: 'Erstellen dein\nUnternehmensprofil',
                      secondStepTitle: 'Erstellen ein Jobinserat',
                      thirdStepTitle: 'Wähle deinen\nneuen Mitarbeiter aus',
                      secondImagePath: '5',
                      thirdImagePath: '6',
                    ),
                    TabBarViewDesktopItem(
                      title:
                          'Drei einfache Schritte zur\nVermittlung neuer Mitarbeiter',
                      firstStepTitle: 'Erstellen dein\nUnternehmensprofil',
                      secondStepTitle:
                          'Erhalte Vermittlungs-\nangebot von Arbeitgeber',
                      thirdStepTitle:
                          'Vermittlung nach\nProvision oder\nStundenlohn',
                      secondImagePath: '7',
                      thirdImagePath: '8',
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
