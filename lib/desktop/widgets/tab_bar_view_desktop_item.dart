import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';

class TabBarViewDesktopItem extends StatelessWidget {
  const TabBarViewDesktopItem({
    super.key,
    required this.title,
    required this.firstStepTitle,
    required this.secondStepTitle,
    required this.thirdStepTitle,
    required this.secondImagePath,
    required this.thirdImagePath,
  });

  final String title;
  final String firstStepTitle;
  final String secondStepTitle;
  final String thirdStepTitle;
  final String secondImagePath;
  final String thirdImagePath;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    return Stack(
      alignment: AlignmentDirectional.center,
      children: [
        Positioned(
          top: 55,
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 40,
              color: Color(0xff4A5568),
              height: 1.2,
            ),
          ),
        ),
        Positioned(
          top: 692,
          child: ClipPath(
            clipper: WaveClipperOne(reverse: true, flip: false),
            child: ClipPath(
              clipper: WaveClipperOne(),
              child: Container(
                height: 370,
                width: width,
                decoration: const BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topRight,
                    end: Alignment.bottomLeft,
                    colors: [
                      Color(0xffEBF4FF),
                      Color(0xffE6FFFA),
                    ],
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(top: 90),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 457.86,
                        ),
                        child: SizedBox(
                          width: 325,
                          child: SvgPicture.asset(
                              'assets/images/$secondImagePath.svg'),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(
                          left: 20,
                        ),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const Text(
                              '2.',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 130,
                                color: Color(0xff718096),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 23, top: 75),
                              child: Text(
                                secondStepTitle,
                                textAlign: TextAlign.left,
                                style: const TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.w600,
                                  color: Color(0xff718096),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Positioned(
          left: 465,
          top: 1234,
          child: Stack(
            alignment: AlignmentDirectional.topCenter,
            clipBehavior: Clip.none,
            children: [
              Container(
                width: 304,
                height: 304,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xffF7FAFC),
                ),
              ),
              const Text(
                '3.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 130,
                  color: Color(0xff718096),
                ),
              ),
              Positioned(
                left: 235,
                top: 55,
                child: Text(
                  thirdStepTitle,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w600,
                    color: Color(0xff718096),
                  ),
                ),
              ),
              Positioned(
                left: 535,
                child: SvgPicture.asset('assets/images/$thirdImagePath.svg'),
              ),
            ],
          ),
        ),
        Positioned(
          top: 1018,
          left: 617,
          child: SvgPicture.asset('assets/images/arrow_2.svg'),
        ),
        Positioned(
          left: 100,
          top: 284,
          child: Stack(
            alignment: AlignmentDirectional.center,
            clipBehavior: Clip.none,
            children: [
              Container(
                width: 208,
                height: 208,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: const Color(0xffF7FAFC).withOpacity(0.5),
                ),
              ),
              const Text(
                '1.',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 130,
                  color: Color(0xff718096),
                ),
              ),
              Positioned(
                left: 171,
                bottom: 50,
                child: Text(
                  firstStepTitle,
                  textAlign: TextAlign.left,
                  style: const TextStyle(
                    fontSize: 30,
                    color: Color(0xff718096),
                  ),
                ),
              ),
              Positioned(
                left: 554.33,
                child: SvgPicture.asset('assets/images/2.svg'),
              ),
            ],
          ),
        ),
        Positioned(
          top: 478.47,
          left: 250,
          child: SvgPicture.asset('assets/images/arrow_1.svg'),
        ),
      ],
    );
  }
}
